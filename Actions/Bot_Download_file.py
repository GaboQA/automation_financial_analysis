import unittest 
import time 
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException
import datetime

#Global Sentences
now = datetime.datetime.now()

class analisis(unittest.TestCase):
    
    
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        
        #print(now.year, now.month)
        prefs = {'download.default_directory' : 'D:\Downloads\ejemplo\{}\{}'.format(now.year,now.month)}
        chrome_options.add_experimental_option('prefs', prefs)
        self.driver = webdriver.Chrome(executable_path=r"C:\Dchroom\R\chromedriver.exe", chrome_options=chrome_options)
        
        
            
    def test_download_file_csv(self):
        driver = self.driver
        driver.get("https://www.sugef.fi.cr/reportes/Informacion_Financiera_Contable.aspx")
        driver.maximize_window()
        WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="ctl00_ContentContainerInside"]/div[3]/div/div/div/div[1]/p[5]/a')))
        buscar = driver.find_element(By.XPATH, '//*[@id="ctl00_ContentContainerInside"]/div[3]/div/div/div/div[1]/p[5]/a')
        driver.execute_script('arguments[0].scrollIntoView();', buscar)
        ##driver.switch_to.frame(driver.find_element_by_xpath('//*[@id="sb-site"]/div[4]'))
        time.sleep(10)
        driver.find_element_by_xpath('//*[@id="ctl00_ContentContainerInside"]/div[3]/div/div/div/div[1]/p[5]/a').click()
        time.sleep(10)
        driver.switch_to.frame(driver.find_element_by_xpath('//*[@id="ctl00_ContentContainerInside"]/div[3]/div/p/span[1]/iframe'))
        time.sleep(10)
        driver.find_element_by_xpath('//*[@id="btnEntidades"]').click()
        Marcar_todos = driver.find_element_by_xpath('//*[@id="btn_searchAreaControl_Popup_2_selectAll"]')
        driver.execute_script('arguments[0].scrollIntoView();', Marcar_todos)
        time.sleep(10)
        driver.find_element(By.XPATH, '//*[@id="btn_searchAreaControl_Popup_2_selectAll"]').click()
        time.sleep(5)
        driver.find_element(By.XPATH, '//*[@id="sa_col_1"]/ul/li[8]/ul/li[1]/span').click()
        driver.find_element(By.XPATH, '//*[@id="sa_col_1"]/ul/li[8]/ul/li[2]/span').click()
        driver.find_element(By.XPATH, '//*[@id="sa_col_1"]/ul/li[8]/ul/li[3]/span').click()
        driver.find_element(By.XPATH, '//*[@id="btn_searchAreaControl_Popup_2_close"]').click()
        driver.implicitly_wait(5)
        # OBTENER MES Y AÑO ACTUAL

        F_ini  = driver.find_element_by_xpath('//*[@id="PeriodosCargados"]')
        F_sec  = driver.find_element_by_xpath('//*[@id="divPeriodoFin"]/div/select')

        # Recorremos los select
        Sl1 = F_ini.find_elements_by_tag_name('option')
        Sl2 = F_sec.find_elements_by_tag_name('option')

        Sl1[1].click()
        time.sleep(5)
        Sl1[0].click()
        # Generamos el reporte
        driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[3]/div[2]/button[1]').click()
        time.sleep(20)
        # Descargamos el documento
        driver.find_element_by_xpath('/html/body/div[2]/div[2]/div[3]/div[2]/button[2]').click()
        time.sleep(20)

        
        
    def test_readFile(self):
        driver = self.driver
        
        
    def tearDown(self):
        self.driver.close()
        
        
        
        
if __name__ == "__main__":
    unittest.main()