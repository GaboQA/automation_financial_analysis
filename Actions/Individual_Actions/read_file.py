import unittest
import time 
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select 
from openpyxl import load_workbook
import csv


class read_file_xlsx(unittest.TestCase):
    
    
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"C:\Dchroom\R\chromedriver.exe")
        
        
    def test_read_file(self):
        driver = self.driver
        time.sleep(3)
        with open('eggs.csv', newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for row in spamreader:
                print(', '.join(row))

    def tearDown(self):
        self.driver.close()
        
        
        
if __name__ == "__main__":
    unittest.main()